package wzq.cn.wuziqi.algorithm;

import wzq.cn.wuziqi.model.ChessBoard;

/**
 * Created by xiawei on 15/8/9.
 */
public class ScoreTable {


    public long[][] tables ;


    public static final int BLACK = 0;
    public static final int WHITE = 1;

    private static ScoreTable blackSideScoreTable = null;
    private static ScoreTable whiteSideScoreTable = null;

    private ScoreTable(int xNumber,int yNumber){
        tables = new long[xNumber][yNumber];
    }

    public static ScoreTable get(ChessBoard.SIDE side,int xNumber,int yNumber){

        if(blackSideScoreTable == null){
            synchronized (ScoreTable.class){
                if(blackSideScoreTable == null)
                blackSideScoreTable = new ScoreTable(xNumber,yNumber);
            }
        }

        if(whiteSideScoreTable == null){
            synchronized (ScoreTable.class) {
                if (whiteSideScoreTable == null)
                    whiteSideScoreTable = new ScoreTable(xNumber, yNumber);
            }
        }

        if(ChessBoard.SIDE.BLACK==side)return blackSideScoreTable;
        else return  whiteSideScoreTable;
    }


    @Override
    public String toString() {
        if(tables==null) return "ScoreTables null";
        else {

            StringBuilder stringBuilder= new StringBuilder();
            for (int j = 0; j < tables.length; j++) {
                for (int k = 0; k < tables[0].length; k++) {
                    stringBuilder.append(tables[j][k]).append(",");
                }
            }
           return  stringBuilder.toString();
        }
    }

    public static void clear() {
        if(whiteSideScoreTable.tables!=null){
            clearTable(whiteSideScoreTable.tables);
        }

        if(blackSideScoreTable.tables!=null){
            clearTable(blackSideScoreTable.tables);
        }
    }

    private static void clearTable(long[][] teSideScoreTable) {
        if(teSideScoreTable !=null){
            for (int i = 0; i <teSideScoreTable.length ; i++) {
                for (int j = 0; j < teSideScoreTable[0].length; j++) {
                    teSideScoreTable[i][j] = -1;
                }
            }
        }
    }
}
