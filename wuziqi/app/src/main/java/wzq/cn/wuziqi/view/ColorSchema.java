package wzq.cn.wuziqi.view;

import android.graphics.Bitmap;

/**
 * Created by xiawei on 15/10/22.
 */
public class ColorSchema {
    //背景色
    public int colorBackground;
    //线的颜色
    public int colorLine;
    //白色球
    public int colorWhite;
    //黑色球
    public int colorBlack;

    //白色方球
    public Bitmap mWhiteBitmap = null;
    //黑色方球
    public Bitmap mBlackBitmap = null;




}
