package wzq.cn.wuziqi.adapter;

import android.util.SparseArray;
import android.view.View;

/**
 * Created by xiawei on 15/8/23.
 */
public class ViewHolder {

    private SparseArray<View> views;
    private View convertView;

    private ViewHolder(View convertView){
        this.views = new SparseArray<View>();
        this.convertView = convertView;
        convertView.setTag(this);
    }

    public static ViewHolder get(View convertView){
        ViewHolder existedHolder = (ViewHolder) convertView.getTag();
        if (existedHolder == null) {
            return new ViewHolder(convertView);
        }
        return existedHolder;
    }

    public <T extends View> T getView(int viewId) {
        View view = views.get(viewId);
        if (view == null) {
            view = convertView.findViewById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }
}