package wzq.cn.wuziqi.view;

import android.content.Context;

import java.util.ArrayList;

import wzq.cn.wuziqi.R;

/**
 * Created by xiawei on 15/10/22.，自定义棋子的现实
 */
public class ColorSchemaList {
    public static ArrayList<ColorSchema> schemas = new ArrayList<>();

    public static  void init(Context ctx){
        ColorSchema colorSchema = new ColorSchema();
        colorSchema.colorBackground= R.color.default_background;
        colorSchema.colorLine=R.color.main_black;
        colorSchema.colorWhite = R.color.main_white;
        colorSchema.colorBlack = R.color.main_black;
        schemas.add(colorSchema);
    }



}
