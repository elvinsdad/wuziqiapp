package wzq.cn.wuziqi.algorithm;

import wzq.cn.wuziqi.model.ChessBoard;
import wzq.cn.wuziqi.model.PointU;
import wzq.cn.wuziqi.model.SuccessResult;

/**
 * Created by xiawei on 15/8/9.
 * 核心算法
 */
public class CoreAlgorithm {
    /**
     * 单个 55 的结果集数量
     **/
    public static final int SINGLE_5_5_COUNT = 12;
    public static final int SUCCESS_NUMBER = 5;


    static SuccessResult[] singles = new SuccessResult[12];

    static {
        // v 0
        singles[0] = new SuccessResult(new PointU(0, 0), new PointU(0, 1), new PointU(0, 2), new PointU(0, 3), new PointU(0, 4));
        //v1
        singles[1] = new SuccessResult(new PointU(1, 0), new PointU(1, 1), new PointU(1, 2), new PointU(1, 3), new PointU(1, 4));
        singles[2] = new SuccessResult(new PointU(2, 0), new PointU(2, 1), new PointU(2, 2), new PointU(2, 3), new PointU(2, 4));
        singles[3] = new SuccessResult(new PointU(3, 0), new PointU(3, 1), new PointU(3, 2), new PointU(3, 3), new PointU(3, 4));
        singles[4] = new SuccessResult(new PointU(4, 0), new PointU(4, 1), new PointU(4, 2), new PointU(4, 3), new PointU(4, 4));
        //  h0 - h4
        singles[5] = new SuccessResult(new PointU(0, 0), new PointU(1, 0), new PointU(2, 0), new PointU(3, 0), new PointU(4, 0));
        singles[6] = new SuccessResult(new PointU(0, 1), new PointU(1, 1), new PointU(2, 1), new PointU(3, 1), new PointU(4, 1));
        singles[7] = new SuccessResult(new PointU(0, 2), new PointU(1, 2), new PointU(2, 2), new PointU(3, 2), new PointU(4, 2));
        singles[8] = new SuccessResult(new PointU(0, 3), new PointU(1, 3), new PointU(2, 3), new PointU(3, 3), new PointU(4, 3));
        singles[9] = new SuccessResult(new PointU(0, 4), new PointU(1, 4), new PointU(2, 4), new PointU(3, 4), new PointU(4, 4));
        // \
        singles[10] = new SuccessResult(new PointU(0, 0), new PointU(1, 1), new PointU(2, 2), new PointU(3, 3), new PointU(4, 4));
        // /
        singles[11] = new SuccessResult(new PointU(4, 0), new PointU(3, 1), new PointU(2, 2), new PointU(1, 3), new PointU(0, 4));

    }


    /**
     * 遍历出所有获胜的结果
     *
     * @param xNumber
     * @param yNumber
     * @return
     */
    public static SuccessResult[] getAllSuccessResults(int xNumber, int yNumber) {

//        if(xNumber < SUCCESS_NUMBER || yNumber <SUCCESS_NUMBER){
//            xNumber = ChessBoard.chessBoardxNumber;
//            yNumber = ChessBoard.chessBoardyNumber;
//        }else{
//            ChessBoard.chessBoardxNumber = xNumber;
//            chessBoardyNumber = yNumber;
//        }

        int count = getSuccessResultCount(xNumber, yNumber);
        SuccessResult[] r = new SuccessResult[count];

        /**
         * public static void arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
         */
        // System.arraycopy(singles,0,r,0,12)
        int deltaX = xNumber - SUCCESS_NUMBER + 1;
        int deltaY = yNumber - SUCCESS_NUMBER + 1;
        for (int i = 0; i < deltaX; i++) {
            for (int j = 0; j < deltaY; j++) {
                for (int k = 0; k < 12; k++) {
                    // x =2 ,y=3
                    // 00 01 02 10 11 12 //0 1 2 3 4 5
                    // i*x + j
                    int number = ((i * deltaX) + j) * SINGLE_5_5_COUNT + k;
                    r[number] = singles[k].move(i, j);
                    //  Log.d("tag",r[number].toString()+"i="+i+"j="+j+"k="+k+"number="+number);
                }
            }
        }
        return r;
    }

    /**
     * 获取结果集的数量
     *
     * @param xNumber x  point number
     * @param yNumber y
     * @return 数量
     */
    public static int getSuccessResultCount(int xNumber, int yNumber) {
        return SINGLE_5_5_COUNT * ((xNumber - SUCCESS_NUMBER + 1) * (yNumber - SUCCESS_NUMBER + 1));
    }


    /**
     * 计算出下一步的位置
     *
     * @param otherSideCurrentPoint 对方下了哪一步
     * @param cb
     * @return
     */
    public static PointU getNexPoint(ChessBoard cb, PointU otherSideCurrentPoint) {
        PointU p = new PointU();
        PointU pc = otherSideCurrentPoint;

        //修改得分表,循环每一个各自
        /**
         * 循环每一个各自，计算可以目前获胜的数量
         */
        for (int i = 0; i < cb.chessBoardxNumber; i++) {
            for (int j = 0; j < cb.chessBoardyNumber; j++) {
                for (int k = 0; k < cb.results.length; k++) {
                    SuccessResult sr = cb.results[k];
                    if (sr.contains(pc)) {
                        sr.isEnForMan = false;
                    }
                    PointU desk_point = cb.mDeskResult[i][j];
                    //没有被设置，并且可以连成5个
                    if(sr.contains(desk_point)&&!desk_point.isset()){
                       //cb.firstSideTable
                    }
                }
            }
        }
        return p;
    }

}
