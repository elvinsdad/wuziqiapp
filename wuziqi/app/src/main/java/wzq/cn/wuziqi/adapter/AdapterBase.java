package wzq.cn.wuziqi.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

public abstract class AdapterBase<T> extends BaseAdapter {

    public final ArrayList<T> mList = new ArrayList<T>();

    public ArrayList<T> getList() {
        return mList;
    }


    public void appendToList(ArrayList<T> list) {
        if (list == null) {
            return;
        }
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addToList(T list) {
        if (list == null) {
            return;
        }
        mList.add(list);
        notifyDataSetChanged();
    }

    public void addToTop(T list) {
        if (list == null) {
            return;
        }
        mList.add(0, list);
        notifyDataSetChanged();
    }

    public void clearAppendToList(ArrayList<T> list) {
        if (list == null) {
            return;
        }
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }


    public void appendToTopList(ArrayList<T> list) {
        if (list == null) {
            return;
        }
        mList.addAll(0, list);
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (position > mList.size() - 1) {
            return null;
        }
        return (T) mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == getCount() - 1) {
            onReachBottom();
        }
        return getExView(position, convertView, parent);
    }

    protected abstract View getExView(int position, View convertView, ViewGroup parent);

    protected void onReachBottom() {
    }


}
