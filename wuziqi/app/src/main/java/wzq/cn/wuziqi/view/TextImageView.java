package wzq.cn.wuziqi.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by xiawei on 15/11/16.
 */
public class TextImageView extends ImageView{


    private String text = "飞龙在天";
    private Paint mPaint;
    public TextImageView(Context context) {
        this(context, null);
    }

    public TextImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setTypeface(Typeface.SERIF);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width  = getWidth();
        int height = getHeight();

        int size = (Math.min(width,height));
        mPaint.setTextSize(size/3);
        if(text !=null && text.length()>0)
        canvas.drawText(text,0,1,0,size*2/3,mPaint);
    }
}
