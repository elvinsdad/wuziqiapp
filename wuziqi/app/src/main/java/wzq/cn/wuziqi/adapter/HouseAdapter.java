package wzq.cn.wuziqi.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import wzq.cn.wuziqi.R;
import wzq.cn.wuziqi.RC;
import wzq.cn.wuziqi.com.lion.material.widget.LLinearLayout;
import wzq.cn.wuziqi.model.House;
import wzq.cn.wuziqi.utils.Utils;

/**
 * Created by xiawei on 15/11/16.
 */
public class HouseAdapter extends BaseRecylerViewAdapter {


    private ArrayList<House> houses = new ArrayList<>();
    private RecyclerView recyclerView;
    private BaseRecylerViewAdapter.RecylerViewItemClickListener itemClickListener;
    private Activity activity;

    public HouseAdapter(Activity activity, RecyclerView recyclerView, RecylerViewItemClickListener itemClickListener) {
        this.recyclerView = recyclerView;
        this.activity = activity;
        this.itemClickListener = itemClickListener;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public void setItemClickListener(RecylerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public HouseAdapter(Activity activity) {
        this(activity, null, null);
    }

    public void clearAndAppend(ArrayList<House> games) {
        this.houses.clear();
        this.houses.addAll(games);
        notifyDataSetChanged();
    }

    @Override
    public RecylerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_house, null);
        LLinearLayout lf = (LLinearLayout) view.findViewById(R.id.lf_backgroud);
        lf.setColor(RC.getRandomColor());
        RecylerViewHolder holder = new RecylerViewHolder(view);
        holder.setItemClickListener(this.itemClickListener);
        return holder;
    }


    /**
     * 从ViewHolder将VIew 取出，并赋值
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecylerViewHolder viewHolder, int position) {
        int wid = Utils.getScreentWidth(activity);
        ViewGroup.LayoutParams ll = new ViewGroup.LayoutParams(wid / 4, wid / 4);
        viewHolder.getConvertView().setLayoutParams(ll);
        //FrameLayout lf_container = viewHolder.getView(R.id.lf_container);
        //lf_container.setBackgroundColor(RC.getRandomColor());

        TextView tv_host = viewHolder.getView(R.id.tv_host);
        TextView tv_client = viewHolder.getView(R.id.tv_client);

        tv_host.setText(RC.getRandomNames());
        tv_client.setText(RC.getRandomNames());
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    public RecylerViewItemClickListener getItemClickListener() {
        return itemClickListener;
    }

}
