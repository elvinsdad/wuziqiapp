package wzq.cn.wuziqi.model;

import java.util.LinkedList;

/**
 * Created by xiawei on 15/10/26.
 */
public class House {

    public Side mWhiteSide;
    public Side mBlackSide;
    public LinkedList<Side> mWatchers;
    public STATUS status;
    public long mCreateTime;


    public void  reset(){
        mWhiteSide = null;
        mBlackSide = null;
        mWatchers = null;
        status = STATUS.FREE;
        mCreateTime = 0L;
    }

    public void join(Side side){
        if(side==null){
            return ;
        }

        side.joinTimeStamp = System.currentTimeMillis();
        if(mWhiteSide == null){
            mWhiteSide = side;
        }else if(mBlackSide == null){
            mBlackSide = side;
        }else{
            if(mWatchers == null){
                mWatchers = new LinkedList<>();
            }
            mWatchers.add(side);
        }
    }


    public enum STATUS{
       FREE,//空闲
        WAITING,//等待
        PLAYING,//游戏中
        LOCKED,//锁定
        USERLESS//不可用
    }

}
