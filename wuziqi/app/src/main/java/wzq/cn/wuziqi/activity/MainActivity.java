package wzq.cn.wuziqi.activity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import wzq.cn.wuziqi.R;
import wzq.cn.wuziqi.adapter.BaseRecylerViewAdapter;
import wzq.cn.wuziqi.adapter.HouseAdapter;
import wzq.cn.wuziqi.com.lion.material.widget.LDrawerButton;

public class MainActivity extends BaseActivity implements View.OnClickListener, DrawerLayout.DrawerListener, BaseRecylerViewAdapter.RecylerViewItemClickListener {


    private LDrawerButton header_left;
    private DrawerLayout drawer_layout;
    private TextView tv_title;
    private RecyclerView recyclerView;
    // private SwipeRefreshLayout swipe_refresh_view;
    private GridLayoutManager mLayoutManager;
    private HouseAdapter mHouseAdapter;


    private LinearLayout game_container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        header_left = getView(R.id.header_left);
        header_left.setOnClickListener(this);
        drawer_layout = getView(R.id.drawer_layout);
        drawer_layout.setDrawerListener(this);
        tv_title = getView(R.id.tv_title);
        recyclerView = getView(R.id.recyler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new GridLayoutManager(this, 4);
        mLayoutManager.setRecycleChildrenOnDetach(true);
        mLayoutManager.offsetChildrenHorizontal(4);
        mLayoutManager.offsetChildrenVertical(4);
        recyclerView.setLayoutManager(mLayoutManager);
        mHouseAdapter = new HouseAdapter(this, recyclerView, this);
        recyclerView.setAdapter(mHouseAdapter);
        game_container = (LinearLayout) findViewById(R.id.game_container);

    }


    /**
     *
     */
    private void loadHouse() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header_left:
                if (drawer_layout.isDrawerOpen(Gravity.LEFT)) {
                    drawer_layout.closeDrawer(Gravity.LEFT);
                } else {
                    drawer_layout.openDrawer(Gravity.LEFT);
                }
                break;
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        header_left.onDrag(drawer_layout.isDrawerOpen(Gravity.LEFT), slideOffset);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
    }

    @Override
    public void onDrawerClosed(View drawerView) {
    }

    @Override
    public void onDrawerStateChanged(int newState) {
    }


    /**
     * 点击某个房间，进入
     *
     * @param view
     * @param position
     */
    @Override
    public void onItemClick(View view, int position) {
        Log.d("onItemClick", "p=" + position);
    }
}
