package wzq.cn.wuziqi.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import wzq.cn.wuziqi.R;
import wzq.cn.wuziqi.RC;
import wzq.cn.wuziqi.core.AI;
import wzq.cn.wuziqi.model.ChessBoard;
import wzq.cn.wuziqi.model.Player;
import wzq.cn.wuziqi.model.PointU;
import wzq.cn.wuziqi.utils.LogUtils;
import wzq.cn.wuziqi.view.SurfaceViewTempalte;

/**
 * Created by xiawei on 15/10/22.
 */
public class GameFragment extends Fragment implements View.OnClickListener {


    private SurfaceViewTempalte mMainSurfaceView;
    private Button mBtnRetract;
    private ChessBoard mChessBoard;
    private Player player;
    private Player player1;
    private View parentView;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.activity_game,container);
        init();
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    private BroadcastReceiver receiver =  new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(RC.ACTION_SUCCESS)){
                    ChessBoard.SIDE side = (ChessBoard.SIDE) intent.getSerializableExtra(RC.KEY_SIDE);
                    Toast.makeText(context,side+"success",Toast.LENGTH_SHORT).show();
                }
        }
    };

    public void restart(View v){
        mChessBoard.restart(ChessBoard.SIDE.BLACK);
    }

    private  AI ai = new AI();
    /**
     *  ai走一步
     * @param v
     */
    public void aigo(View v){

        if(mChessBoard.mCurrentSide.side == mChessBoard.twoSide[1].side) {
            PointU p = ai.getNextStep(mChessBoard.mDeskResult, mChessBoard.successNumber);
            mChessBoard.onMoveFinish(p.x, p.y);
        }else{
            LogUtils.d("bushi AI ZOU");
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }



    private void init() {
        player =new Player();
        player.userId = 1;
        player.nickname = "p1";
        player1 =new Player();
        player1.userId = 2;
        player1.nickname = "p2";
        initView();
        initContext();
    }

    private void initContext() {
        mChessBoard = new ChessBoard(getActivity(),10,10);
        mChessBoard.setmType(ChessBoard.TYPE.AI);
        mChessBoard.join(player);
        mChessBoard.setFirstSide(0);
        mChessBoard.join(player1);
        mChessBoard.registerObserver(mMainSurfaceView);
        mMainSurfaceView.registerFinishListener(mChessBoard);
    }

    private void initView() {
        mBtnRetract = (Button) parentView.findViewById(R.id.btn_retract);
        mBtnRetract.setOnClickListener(this);
        mMainSurfaceView = (SurfaceViewTempalte) parentView.findViewById(R.id.surfacebiew);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_retract:
                //悔棋
                mChessBoard.notifyAction(ChessBoard.ACTION.RETRACT,mChessBoard.mCurrentSide.side);
                break;
        }
    }
}
