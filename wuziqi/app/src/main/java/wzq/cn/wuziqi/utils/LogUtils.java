package wzq.cn.wuziqi.utils;


import android.util.Log;

public class LogUtils {

	private static int LEVEL = 0;//  
	
	private final static int VERBOSE = Log.VERBOSE;//2
	private final static int DEBUG = Log.DEBUG;//3
	private final static int INFO = Log.INFO;//4
	private final static int WARN = Log.WARN;//5
	private final static int ERROR = Log.ERROR;//6
	
	public static void v(String tag,String msg){
		if(LEVEL < VERBOSE){
			Log.v(tag, msg);
		}
	}
	
	public static void d(String tag,String msg){
		if(LEVEL < DEBUG){
			Log.i(tag, msg);
		}
		
	}

	public static void d(String msg){
		if(LEVEL < DEBUG){
			Log.i("pintu", msg);
		}

	}
	
	public static void i(String tag,String msg){
		if(LEVEL < INFO){
			Log.i(tag, msg);
		}
	}
	
	public static void w(String tag,String msg){
		if(LEVEL < WARN){
			Log.i(tag, msg);
		}
	}
	
	public static void e(String tag,String msg){
		if(LEVEL < ERROR){
			Log.i(tag, msg);
		}
	}
	

}
