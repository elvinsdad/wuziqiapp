package wzq.cn.wuziqi.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Objects;

public class JSONPaser {


    public static <T> ArrayList<T> getList(JSONArray array, Class<T> clazz) {
        int count = 0;
        if (array != null) {
            count = array.length();
        }
        ArrayList<T> lists = new ArrayList<T>();
        for (int i = 0; i < count; i++) {
            try {
                JSONObject jsonObject = array.getJSONObject(i);
                T t = getObjectFromJson(jsonObject, clazz);
                if (t != null) {
                    lists.add(t);
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return lists;
    }

    /**
     * 解析json数组列表
     *
     * @param result
     * @return
     */
    public static JSONArray getJsonList(String result) {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(result);
        } catch (JSONException e) {
            return null;
        }
        return jsonArray;
    }

    /**
     * 获取
     *
     * @param srcObj
     * @param name
     * @return
     */
    public static JSONObject getJSONObject(JSONObject srcObj, String name) {
        if (srcObj == null) {
            return null;
        } else {
            try {
                return srcObj.getJSONObject(name);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * 获取
     *
     * @param srcObj
     * @param p
     * @return
     */
    public static JSONObject getJSONObject(JSONArray srcObj, int p) {
        if (srcObj == null) {
            return null;
        } else {
            try {
                return srcObj.getJSONObject(p);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String getString(JSONObject o, String name) {
        if (o == null) {
            return "";
        }
        String s = "";
        try {
            s = o.getString(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static int getInt(JSONObject o, String name) {
        if (o == null) {
            return 0;
        }
       int s = 0;
        try {
            s = o.getInt(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static <T> T getObjectFromJson(String json, Class<T> clazz) {
        JSONObject o;
        try {
            o = new JSONObject(json);
            return getObjectFromJson(o, clazz);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * JSONOBJECT 转化为 obj
     *
     * @param o
     * @param clazz
     * @return
     */
    public static <T> T getObjectFromJson(JSONObject o, Class<T> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        T t = null;
        try {

            t = clazz.newInstance();
            for (Field field : fields) {
                boolean isStatic = Modifier.isStatic(field.getModifiers());
                if (!isStatic) {
                    if (field.getType() == String.class) {
                        // 只获取非静态变量
                        String fieldname = field.getName();
                        String value = getString(o, fieldname);
                        setTProperty(t, fieldname, value);
                    }else if(field.getType() == Integer.class || field.getType()==int.class){
                        // 只获取非静态变量
                        String fieldname = field.getName();
                        int value = getInt(o,fieldname);
                        setTProperty(t, fieldname, value);
                    }
                }
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return t;
    }

    public static void setTProperty(Object o, String fieldname, Object value) {
        try {

            Field field = o.getClass().getDeclaredField(fieldname);
            field.setAccessible(true);
            field.set(o, value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    /**
     * 转化为json
     * @param obj
     */
    public static void toJson(Objects obj){

    }


    /**
     * 将jsonObj 复制到 obj
     *
     * @param o
     * @param obj
     */
    public static void setObject(JSONObject o, Object obj) {
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            boolean isStatic = Modifier.isStatic(field.getModifiers());
            if (!isStatic) {
                if (field.getGenericType().toString()
                        .equals("class java.lang.String")) {
                    // 只获取非静态变量
                    String fieldname = field.getName();
                    String value = getString(o, fieldname);
                    setTProperty(obj, fieldname, value);
                }
            }
        }
    }
}