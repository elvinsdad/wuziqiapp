package wzq.cn.wuziqi.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Objects;

/**
 * Created by xiawei on 15/9/18.
 */
public class PreferenceUtils {

    public static final String sp_name = "sp_pintu";


    public static void putString(Context ctx,String key,String value){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(sp_name,Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(key,value).commit();
    }

    public static void putInt(Context ctx,String key,int value){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(sp_name,Context.MODE_PRIVATE);
        sharedPreferences.edit().putInt(key, value).commit();
    }

    public static final String getString(Context ctx,String key,String def){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(sp_name,Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,def);
    }


    public static final String getString(Context ctx,String key){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(sp_name,Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,"");
    }

    /**
     * 保存单例对象
     * @param cxt
     * @param obj
     */
    public static final void putObject(Context ctx,Objects obj){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(sp_name,Context.MODE_PRIVATE);

    }





}
