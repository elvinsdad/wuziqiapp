package wzq.cn.wuziqi;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import wzq.cn.wuziqi.view.ColorSchemaList;


public class App extends Application {


    private static RefWatcher refWatcherl;

    public static RefWatcher getWatcher() {
        return refWatcherl;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Crash.getInstance().init(getApplicationContext());
        ColorSchemaList.init(getApplicationContext());
        refWatcherl = LeakCanary.install(this);
    }


}
