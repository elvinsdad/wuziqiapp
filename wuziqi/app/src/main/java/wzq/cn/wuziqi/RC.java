package wzq.cn.wuziqi;

import java.util.Random;

import wzq.cn.wuziqi.model.ChessBoard;

/**
 * Created by xiawei on 15/8/9.
 * 定义常量
 */
public class RC {

    public static final String sp_name = "wuziqi";


    public static final int SIDE_FIRST = -1;

    public static final int SIDE_SECOND = 1;

    public static final String ACTION_SUCCESS = "ACTION_SUCCESS";
    public static final String KEY_SIDE = "side";
    public static final int MAX_ROW = 15;


    /**
     * 得分表  1得1分，2连子100分，3连子。。。。
     */
    public static long[] SCORES = new long[10];

    static {
        for (int i = 0; i < 10; i++) {
            if (i == 0) {
                SCORES[0] = 1;
            } else {
                SCORES[i] = SCORES[i - 1] * 100;
            }
        }
    }

    /**
     * 8个方向,全局配置
     */
    public static ChessBoard.DIRECT[] directs = new ChessBoard.DIRECT[]{
            ChessBoard.DIRECT.BUTTOM, ChessBoard.DIRECT.BUTTON_LEFT,
            ChessBoard.DIRECT.RIGHT_BUTTON, ChessBoard.DIRECT.RIGHT,
            ChessBoard.DIRECT.TOP_RIGHT, ChessBoard.DIRECT.LEFT,
            ChessBoard.DIRECT.LEFT_TOP, ChessBoard.DIRECT.TOP
    };


    public static final int[] color_pool = new int[]{0x55ff0000, 0x5500ff00, 0x5500ffff, 0x550000ff,
            0x55ff00ff, 0x551d2088, 0x55e4007f, 0x55fff100, 0x55004986, 0x5566ff00, 0x550036ff, 0x55fc00ff};


    /**
     * 获取一个随机的颜色
     *
     * @return
     */
    public static int getRandomColor() {
        Random random = new Random();
        return color_pool[random
                .nextInt(color_pool.length) % color_pool.length];
    }

    public static String[] ns = new String[]{"亢龙有悔", "飞龙在天", "见龙在田", "鸿渐于陆", "潜龙勿用", "利涉大川", "突如其来"
            , "震惊百里", "或跃在渊", "双龙取水", "鱼跃于渊", "时乘六龙", "密云不雨", "损则有孚"
            , "龙战于野", "履霜冰至", "羝羊触蕃", "神龙摆尾"};


    /**
     * 获取一个随机的颜色
     *
     * @return
     */
    public static String getRandomNames() {
        Random random = new Random();
        return ns[random
                .nextInt(ns.length) % ns.length];
    }


}


