package wzq.cn.wuziqi.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import wzq.cn.wuziqi.App;

/**
 * Created by xiawei on 15/8/9.
 */
public class BaseActivity extends FragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getWatcher().watch(this);
    }

    public <T> T getView(int id) {
        return (T) findViewById(id);
    }
}
