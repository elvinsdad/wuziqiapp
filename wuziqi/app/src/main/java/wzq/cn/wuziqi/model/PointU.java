package wzq.cn.wuziqi.model;

/**
 * Created by xiawei on 15/8/9.
 * 棋盘上的点，不区分对战放
 */
public class PointU {


    public static final int P_UNUSE= -1;
    public int x;
    public int y;
    public long score = 0;
    public ChessBoard.SIDE belong  = ChessBoard.SIDE.NONE;
    public PointU(int x, int y) {
        this.x = x;
        this.y = y;

    }


    @Override
    public String toString() {
        return "PointU{" +
                "x=" + x +
                ", y=" + y +
                ", score=" + score +
                ", belong=" + belong +
                '}';
    }

    /**
     * 是否有棋子
     * @return -1+-1 = -2 《 0
     */
    public boolean isset(){
        return (x+y)>= 0 ? true:false;
    }
    public PointU() {

        this.x = P_UNUSE;
        this.y = P_UNUSE;
    }


}
