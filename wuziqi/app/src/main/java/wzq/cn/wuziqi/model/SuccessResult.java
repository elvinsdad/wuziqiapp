package wzq.cn.wuziqi.model;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by xiawei on 15/8/9.
 * 获胜的结果,单个
 */
public class SuccessResult {

    /**
     * 获胜结果是否对黑方有效
     */
    public boolean isEnForMan = true;
    //对白方是否有效
    public boolean isEnForPc = true;
    //这个获胜方式的五个坐标值

    public PointU[] getResults() {
        return results;
    }

    public void setResults(PointU[] results) {
        this.results = results;
    }

    public LinkedList<PointU> rs = new LinkedList<>();
    public PointU[] results = new PointU[]{new PointU(), new PointU(), new PointU(), new PointU(), new PointU()};

    public SuccessResult() {
    }

    public LinkedList<PointU> getRs() {
        return rs;
    }

    public void setRs(LinkedList<PointU> rs) {
        this.rs = rs;
    }

    @Override
    public String toString() {
        return "SuccessResult{" +
                "isEnForBlack=" + isEnForMan +
                ", isEnForWhite=" + isEnForPc +
                ", results=" + Arrays.toString(results) +
                '}';
    }

    public SuccessResult(PointU p0, PointU p1, PointU p2, PointU p3, PointU p4) {

        this.results[0] = p0;
        this.results[1] = p1;
        this.results[2] = p2;
        this.results[3] = p3;
        this.results[4] = p4;

    }

    /**
     * 作为整体移动，5个点按照xy方向移动
     *
     * @param deltaX
     * @param deltaY
     */
    public SuccessResult move(int deltaX, int deltaY) {
        SuccessResult sr = new SuccessResult();
        for (int i = 0; i < 5; i++) {
            sr.results[i].x = this.results[i].x + deltaX;
            sr.results[i].y = this.results[i].y + deltaY;
        }
        return sr;
    }

    /**
     *
     * @param p
     * @return
     */
    public boolean contains(PointU p) {
       return  contains(p.x,p.y);
    }

    /**
     *
     * @param x,y
     * @return
     */
    public boolean contains(int x,int y) {
        for (int i = 0; i < 5; i++) {
            if(this.results[i].x==x && this.results[i].y==y){
                return  true;
            }
        }
        return false;
    }
}
