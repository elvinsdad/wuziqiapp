package wzq.cn.wuziqi.model;

/**
 * Created by xiaw on 2015/11/17 0017.
 */
public class MainMenu {

    public int id;
    public String name;
    public String icon;
    public int enable;

    @Override
    public String toString() {
        return "MainMenu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", enable=" + enable +
                '}';
    }
}
