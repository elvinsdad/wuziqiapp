package wzq.cn.wuziqi.model;

import android.util.SparseArray;

/** 首页大厅
 * Created by xiawei on 15/10/26.
 */
public class GameHall {

    public SparseArray<House> mHouse;


    public static GameHall gameHall = new GameHall();


    private GameHall(){
        init();
    }

    private void init() {
        mHouse = new SparseArray<>();
    }

    public static GameHall getGameHall() {
        return gameHall;
    }


    public void loadHouses(){

    }

}
