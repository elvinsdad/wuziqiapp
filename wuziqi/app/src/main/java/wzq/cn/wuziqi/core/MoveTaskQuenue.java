package wzq.cn.wuziqi.core;

import java.util.LinkedList;

import wzq.cn.wuziqi.model.ChessBoard;
import wzq.cn.wuziqi.model.PointU;
import wzq.cn.wuziqi.model.Side;

/** 移动队列,两边来回切换，保存历史记录
 * Created by xiawei on 15/10/22.
 */
public class MoveTaskQuenue {

    public static LinkedList<Move> historyMoves = new LinkedList<>();

    public static void add(Move m){
        historyMoves.addLast(m);
    }

    public static class Move{
        public Side who;
        public ChessBoard.ACTION action= ChessBoard.ACTION.NORMAL;
        public PointU point;
    }



}
