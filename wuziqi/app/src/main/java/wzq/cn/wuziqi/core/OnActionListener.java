package wzq.cn.wuziqi.core;

import wzq.cn.wuziqi.model.ChessBoard;

/**
 * Created by xiawei on 15/10/23.
 */
public interface OnActionListener {

     public void onAction(ChessBoard.ACTION action,ChessBoard.SIDE side);
}
