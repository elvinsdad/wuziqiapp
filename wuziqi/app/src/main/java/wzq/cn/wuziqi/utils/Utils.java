package wzq.cn.wuziqi.utils;

import android.content.Context;
import android.view.WindowManager;

/**
 * Created by xiawei on 15/10/22.
 */
public class Utils {

    /**
     * 获取屏幕宽度，px
     * @param ctx
     * @return
     */
    public static int getScreentWidth(Context ctx){
        WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        return  windowManager.getDefaultDisplay().getWidth();
    }

    /**
     * 截取最高2位，10进制
     * @param l
     * @return
     */
    public static int getTop2(long l){
        while (l/10>100){
            l /=10;
        }
        return (int) (l/10);
    }

}
