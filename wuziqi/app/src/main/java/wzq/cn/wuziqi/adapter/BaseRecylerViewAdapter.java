package wzq.cn.wuziqi.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by xiawei on 15/9/8.
 */
public class BaseRecylerViewAdapter extends RecyclerView.Adapter<RecylerViewHolder> {

    private RecylerViewItemClickListener mItemClickListener;

    @Override
    public RecylerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecylerViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
    /**
     * 设置Item点击监听
     *
     * @param listener
     */
    public void setOnItemClickListener(RecylerViewItemClickListener listener) {
        this.mItemClickListener = listener;
    }

    public interface RecylerViewItemClickListener {
        public void onItemClick(View view, int position);
    }


}
