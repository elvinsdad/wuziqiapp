package wzq.cn.wuziqi.core;

import wzq.cn.wuziqi.model.ChessBoard;

/** 下完一步 坚挺
 * Created by xiawei on 15/10/23.
 */
public interface OnMoveFinishedListener {


    public void onMoveFinish(int x,int y);

    public ChessBoard getBoard();


}
