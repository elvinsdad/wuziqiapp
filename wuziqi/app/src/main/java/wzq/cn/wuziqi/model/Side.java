package wzq.cn.wuziqi.model;

/**
 * Created by xiawei on 15/10/23.
 */
public class Side {
    public ChessBoard.SIDE side = ChessBoard.SIDE.NONE;
    public int timeCount = 0;
    public Player player = null;
    public long joinTimeStamp;//加入时间

    @Override
    public String toString() {
        return "Side{" +
                "side=" + side +
                ", timeCount=" + timeCount +
                ", player=" + player +
                ", joinTimeStamp=" + joinTimeStamp +
                '}';
    }
}
